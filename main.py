def xor(a,b):
    return [(a[i] + b[i]) % 2 for i in range(len(a))]

def multiplication(A,B,irr,d=3):    

    result = [ 0 for _ in range(len(A))  ]
    for i in range(len(A)):
        if B[i] == 1:
            shift = A
            for s in range(i):
                do_we_have_overflow = (shift[-1] == 1)
                shift = [0] + shift[:-1]
                if do_we_have_overflow:
                    shift = xor(shift, irr)
            result = xor(result,shift)
    return result

def fx(n,irr):
    
    try:
        lst=[]
        for i in range(0,pow(2,n),1):
            i=bin(i).replace("0b","")
            
            if len(i) < n:
                i=list(map(int,("0"*(n-len(i))+i)))
            if len(irr) < n:
                irr=list(map(int,(irr+"0"*(n-len(irr)))))

            else : i = list(map(int,i))
            
            tmp= multiplication(i,i,irr)
            tmp = multiplication(tmp,i,irr)
            
            
            tmp="".join(list(map(str,tmp)))
            lst.append(tmp)

        return lst
    except:
        print ("check irreducable vector size, and if it irreducable")
def main():        
    degree=int(input("enter p (Fe^p):"))
    irr=list(map(int,input("Enter irreducable vector(1+x+x^2+x^3) size= {}: ".format(degree))))
    lst=fx(degree,irr)
    for k,v in enumerate(lst): print("f({}) = {} " .format(bin(k).replace("0b",""),v))

if __name__=="__main__":
    main()

